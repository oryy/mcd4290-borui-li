"use strict";
//This file includes the functions which are the functionalities of the page, these functions will enables user
//to interract with the page and runs the project withput error
let clockElement = document.getElementById("currentTime");

//function time, show the time in index html page, no input and will load time on page 
function time() {
  let d = new Date();
  let s = d.getSeconds();
  let m = d.getMinutes();
  let h = d.getHours();
  clockElement.textContent = ("0" + h).substr(-2) + ":" + ("0" + m).substr(-2) + ":" + ("0" + s).substr(-2);
}
setInterval(time, 1);
//view function, store related info to localstorage for the use of view page, also direct users to the view page.
//input as index and queueindex
function view(index,queueIndex){
        updateLocalStorageData("studentIndex", index);
        updateLocalStorageData("queueIndex", queueIndex);
        window.location.href = "BoruiLi_view.html";
}
//function markdone, make the student as finished with the session and deleate student from the localstorage, also update the page. index and queueindex as input
function markDone(index,queueIndex){
    if(confirm("Are you sure you want to mark this student as Done?")){
        consultSession.removeStudent(index, queueIndex);
        updateLocalStorageData("consultationAppData", consultSession);
        displayQueue(consultSession);
    }
    else{
        return
    }
}
setInterval(displayQueue(consultSession),1000)

//function displayquque, this will load all student in the queue for the session dynamically, input is data
function displayQueue(data){
    let queDiv = document.getElementById("queueContent");
    queDiv.innerHTML="";
    let queData = consultSession.queue;
    for(let i = 0; i < queData.length; i++){
        let num = i + 1;//number of the queue
        queDiv.innerHTML += "<ul class='mdl-list'>" + "<h4>Queue" + num + "</h4>"
        for(let o = 0; o < queData[i].length; o++){
            queDiv.innerHTML += "<li class='mdl-list__item mdl-list__item--three-line'>" + "<span class='mdl-list__item-primary-content'>" + "<i class='material-icons mdl-list__item-avatar'>person</i>" + "<span>" + queData[i][o]._fullName + '</span></span><span class="mdl-list__item-secondary-content"><a class="mdl-list__item-secondary-action" onclick="view(' + o + ',' + i + ')">' + '<i class="material-icons">info</i></a>' + '</span><span class="mdl-list__item-secondary-content">' + '<a class="mdl-list__item-secondary-action" onclick="markDone(' + o + ',' + i + ')">' + '<i class="material-icons">done</i></a></span></li>';
        }
        queDiv.innerHTML += "</ul>";
    }
}


























