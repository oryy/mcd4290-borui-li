"use strict";
//basement code for the assigment, includs the class structure, which is designed to store information in efficient way
//this file also includes some functions which will load the initial condition data for the page, includeing 
//function for local storage
// Keys for localStorage
const STUDENT_INDEX_KEY = "studentIndex";
const STUDENT_QUEUE_KEY = "queueIndex";
const APP_DATA_KEY = "consultationAppData";

class Student{
    constructor(studentFullName, studentId, problemDescription){
        this._fullName = studentFullName;
        this._studentId = studentId;
        this._problem = problemDescription;
    }
    
    get fullName(){
        return this._fullName;
    }
    
    get studentId(){
        return this._studentId;
    }
    
    get problem(){
        return this._problem;
    }
    
    fromData(data){
        this._fullName = data._fullname;
        this._problem = data._problem;
        this._studentId = data._studentId;
    }
}

class Session{
    constructor(){
        this._startTime = {
            'day': "",
            'month': "",
            'time': "",
            'yaer': ""
        }
        this._queue = [];
    }
    
    get startTime(){
        return this._startTime;
    }
    
    get queue(){
        return this._queue;
    }
    
    addSubQueue(subQueue){
        this._queue.push(subQueue);
    }
    
    addStudent(studentFullName, studentId, problemDescription){
        let newStudent = new Student(studentFullName, studentId, problemDescription);
        let minLength=this._queue[0].length
        let minQueue=0
        for(let i = 0; i < this._queue.length; i++){
            if(minLength > this._queue[i].length){
                minLength = this._queue[i].length;
                minQueue = i;
            }
        }
        this._queue[minQueue].push(newStudent);
    }
    
    removeStudent(studentIdInQueue, queueId){
        this._queue[queueId].splice(studentIdInQueue,1);
    }
    
    fromData(data){
        this._queue=data._queue;
        this._startTime=data._startTime;
    }
}
//check localstorage data, check if data exist in local storage, input key and return ture or false
function checkLocalStorageDataExist(key){
    if (localStorage.getItem(key)){
        return true;
    }
    else{
        return false;
    }
}
//updatelocal storage, update the localstorage with the key, input key and data for local storage
function updateLocalStorageData(key, data){
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}
//getlocalstortagedata, get the data from local storage, input is key and return the data according to the key 
function getLocalStorageData(key){
    let info = JSON.parse(localStorage.getItem(key));
    return info;
}

let consultSession = new Session();
if (checkLocalStorageDataExist(APP_DATA_KEY))
{
    // if LS data does exist
    let data = getLocalStorageData(APP_DATA_KEY);
    consultSession.fromData(data);
} 
else
{
    // if LS data doesn’t exist
    consultSession.addSubQueue([]);
    consultSession.addSubQueue([]);
    updateLocalStorageData(APP_DATA_KEY, consultSession);
}






