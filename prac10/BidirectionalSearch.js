/* Write your function for implementing the Bi-directional Search algorithm here */
function  bidirectionalSearchTest(data){
    let middleOfTheArray = data.length / 2;
    for(let i = 0 ; i <= middleOfTheArray ; i++){
        if(data[middleOfTheArray+i].emergency == true){
            return data[middleOfTheArray+i].address
        }
        if(data[middleOfTheArray-i].emergency == true){
            return data[middleOfTheArray-i].address
        }
    }return null
}
