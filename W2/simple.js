//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let a=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,
-51, -17, -25];
    let neg=[];
    let pos=[];
    let pose=[];
    let nego=[];
    for (let i=0;;){
        if (a[i]>0){
            pos.push(a[i])
        }
        else{
            neg.push(a[i])
        }
        i++
        if (i<21){continue}
        else{break}
    }
    for (let i=0;;){
        if (pos[i]%2==1){
            pose.push(pos[i])
        }
        i++
        if (i>9){break}
    }
    for (let i=0;;){
        if (neg[i]%2==0){
            nego.push(neg[i])
        }
        i++
        if (i>9){break}
    }
    output="positive odd:"+pose+"\n"+"negative even:"+nego;
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 

    //Question 2 here 
    
    let n;
    let on=0;
    let tw=0
    let th=0
    let fo=0
    let fi=0
    let si=0
    for (let i=0;i<60000;i++){
       n=Math.floor((Math.random()*6)+1)
        if (n==1){on++}
        else if(n==2){tw++}
        else if(n==3){th++}
        else if(n==4){fo++}
        else if(n==5){fi++}
        else{si++}
        
    }
    output="Frequency of die rolls"+"\n"+"1:"+on+"\n"+"2:"+tw+"\n"+"3:"+th+"\n"+"4:"+fo+"\n"+"5:"+fi+"\n"+"6:"+si
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    
    let frequency=[0,0,0,0,0,0,0];
    let die
        for (let i=0;i<60000;i++){
       die=Math.floor((Math.random()*6)+1)
            frequency[die]++
    }
    output+="Frequency of die rolls"+"\n"
    for (let i=1;i<frequency.length;i++){
        output+=i+":"+frequency[i]+"\n"
    }
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    
    var dieRolls={
        Frequencies:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        }
    }
    let n
    let on=0;
    let tw=0
    let th=0
    let fo=0
    let fi=0
    let si=0
            for (let i=0;i<60000;i++){
       n=Math.floor((Math.random()*6)+1)
        if (n==1){dieRolls.Frequencies={
            1:on++,
            2:tw,
            3:th,
            4:fo,
            5:fi,
            6:si,}}
        else if(n==2){dieRolls.Frequencies={
            1:on,
            2:tw++,
            3:th,
            4:fo,
            5:fi,
            6:si,}}
        else if(n==3){dieRolls.Frequencies={
            1:on,
            2:tw,
            3:th++,
            4:fo,
            5:fi,
            6:si,}}
        else if(n==4){dieRolls.Frequencies={
            1:on,
            2:tw,
            3:th,
            4:fo++,
            5:fi,
            6:si,}}
        else if(n==5){dieRolls.Frequencies={
            1:on,
            2:tw,
            3:th,
            4:fo,
            5:fi++,
            6:si,}}
        else{dieRolls.Frequencies={
            1:on,
            2:tw,
            3:th,
            4:fo,
            5:fi,
            6:si++,}}
            }
    var x, txt = ""
    for (let prop in dieRolls){
                output += prop+" = "+JSON.stringify(dieRolls)+"\n"
    }
//output=JSON.stringify(dieRolls);
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    
let person = {
name: "Jane",
 income: 127050
}
let tax;
    I=person["income"];
    N=person["name"];
    if (I>=0&&I<=18200){
        tax=0
    }
    else if(I>=18210&&I<=37000){
        tax=I*0.19
    }
    else if(I>=37001&&I<=90000){
        tax=3572+0.325*(I-37000)
    }
    else if(I>=90001&&I<=180000){
        tax=20797+0.37*(I-90000)
    }
    else{
        tax=54097+0.45*(I-180000)
    }
    tax=tax.toFixed(2)
    output+=person["name"]+"'s income is: $"+I+", and her tax owed is: "+tax
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}