//JS file for the booking web application

let bookingData = [,,,,,,,,
 {
 time: "08:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "09:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "10:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "11:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "12:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "13:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "14:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "15:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "16:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "17:00",
 reason: "",
 label: "",
 booked: false
 }
];
updateDayTime()

function getObject(data, time) {
    var obj = data.filter(function (item) {
        return item.time == time
    })
    if (obj.length > 0)
        return obj[0]
    return {}
}

function bookRoom(time, reason, label){
    let selectedtime = getObject(bookingData,time)
    selectedtime.reason = reason
    selectedtime.label = label
    selectedtime.booked = true
}

var roomstatu1=""
function checkRoomBooked(time){
     let selectedtime = getObject(bookingData,time)
     let roomstatu = selectedtime.booked
     if(roomstatu == true){
         roomstatu1 = true
         return roomstatu1
     }
    else{roomstatu1 = false
        return roomstatu1
        }
}

function clearRoomBookings(time){
    let selectedtime = getObject(bookingData,time)
    selectedtime.reason = ""
    selectedtime.label = ""
    selectedtime.booked = false 
    
}

//function name updateDisplay
//loop over datalist
//create empty variable availbility
//determine abaibility of each time slot
//assign status of each time slot to variable abaibility
//create html element
//loop end
function updateDisplay(){
for(let i = 8; i < bookingData.length; i++){
    let availbility=""
    let statu = bookingData[i].booked
    if(statu == false){
        availbility="Available"
    }
    else{availbility="not Available"}
    let statuText = document.createTextNode(availbility)
    document.getElementById("output").innerHTML+= "<h4>" + bookingData[i].time + ": " + "<label>"+availbility+"</label>"+"</h4>"
}
}


function doBooking(){
    if(confirm("Are you going to make this booking?")){}
    else{return}
    let reason = document.getElementById("inputReason").value
    if(reason === ""){
        alert("Please use vaild reason")
        return
    }

    let time = document.getElementById("inputTime").value
    if(time === ""){
    alert("Please select vaild time")
    return
    }

    checkRoomBooked(time)
    if(roomstatu1==true){alert("time selected is not availble")
    return
    }
    let label = document.getElementById("inputLabel").value
    if(label === ""){
    alert("Please put a vaild label")
    return
}
    bookRoom(time, reason, label)
    document.getElementById("output").innerHTML = ""
    updateDisplay()
}

function clearAllBookings(){
     if(confirm("Are you going to clear all the bookings?")){}
    else{return}
    for(let i = 8; i < bookingData.length; i++){
    clearRoomBookings(bookingData[i].time)
}
    document.getElementById("output").innerHTML = ""
    updateDisplay()
}


function updateDayTime(){
let today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    document.getElementById("timeNow").innerHTML = time
}


window.onload = function(){setInterval(function(){updateDayTime()},updateDisplay(), 1000)}






